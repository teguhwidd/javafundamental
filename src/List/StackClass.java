package List;

import java.util.EmptyStackException;
import java.util.Stack;

public class StackClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * Stack adalah salah satu struktur data yang memiliki sistem kerja Last In
		 * First Out (LIFO), yang terakhir masuk pertama keluar. Dapat di ilustrasikan
		 * seperti sebuah tumpukan buku, ketika mengambil sebuah buku di dalam tumpukan
		 * itu maka harus diambil satu persatu dari buku yang paling atas dari tumpukan
		 * buku tersebut. Sebuah stack hanya dapat ditambahkan dan dikurangi elemennya
		 * hanya dari satu sisi yakni elemen atasnya atau biasa disebut Top Of Stack.
		 * 
		 * Metode dalam Stack: # Metode init: fungsi yang digunakan untuk inisialisasi
		 * atau membuat stack baru yang masih kosong. # Metode full: digunakan untuk
		 * mengetahui stack penuh atau tidak. # Metode empty: digunakan untuk mengetahui
		 * stack kosong atau tidak. # Metode clear: digunakan untuk mengosongkan stack.
		 * Stack dianggap kosong apabila puncak stack berada pada posisi -1. # Metode
		 * push: digunakan untuk menambahkan data ke dalam stack. Penambahan data tidak
		 * bisa dilakukan apabila stack sudah penuh. Urutan perintahnya adalah:
		 * menambahkan nilai top dan menambahkan data pada posisi nilai top. Jika dalam
		 * Linked List menggunakan method addLast. # Metode pop: digunakan untuk
		 * mengeluarkan data teratas stack dengan syarat bahwa stack tidak kosong.
		 * Urutan perintahnya adalah : menghapus data pada posisi nilai top dan
		 * menurunkan nilai top. Jika dalam Linked List menggunakan method removeLast.
		 */
		Stack1();

	}

	public static void Stack1() {
		// create an Stack
		Stack<String> elektronik = new Stack<String>();
		System.out.println("Stack items before add \t\t\t : " + elektronik);
		
		// add elements to the Stack
		elektronik.add("TV");
		elektronik.add("Laptop");
		elektronik.add("Rice Cooker");
		elektronik.add("Vacum Cleaner");
		elektronik.add("Fan");
		
		// display the stack
		System.out.println("Stack items after add \t\t\t : " + elektronik);
		System.out.println("Stack size \t\t\t\t : " + elektronik.size());
		System.out.println("Ran exist? \t\t\t\t : " + elektronik.contains("Ran"));

		System.out.println("The location of TV \t\t\t : " + elektronik.search("TV"));
		System.out.println("The location of Laptop \t\t\t : " + elektronik.search("Laptop"));
		System.out.println("The location of Rice Cooker \t\t : " + elektronik.search("Rice Cooker"));
		System.out.println("The location of Vacum Cleaner \t\t : " + elektronik.search("Vacum Cleaner"));
		System.out.println("The location of Fan \t\t\t : " + elektronik.search("Fan"));

		System.out.println("Top Stack \t\t\t\t : " + elektronik.peek());

		System.out.println("Pop, " + elektronik.pop() + "\t\t Stack items \t : " + elektronik);
		System.out.println("Pop, " + elektronik.pop() + "\t Stack items \t : " + elektronik);
		System.out.println("Pop, " + elektronik.pop() + "\t Stack items \t : " + elektronik);

		elektronik.clear();
		System.out.println("Clear the Stack,Stack items \t\t : " + elektronik);
		try {
			elektronik.pop();
		} catch (EmptyStackException e) {
			System.out.println("Empty stack");

		}

	}

}
