package List;

import java.text.Collator;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

public class LinkedListClass {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * Pengertian Linked list sebagai berikut:
		 * 
		 * - Sekumpulan elemen bertipe sama, yang mempunyai keterurutan tertentu, yang
		 * setiap elemennya terdiri dari dua bagian. - Struktur berupa rangkaian elemen
		 * saling berkait dimana setiap elemen dihubungkan elemen lain melalui pointer.
		 * Pointer adalah alamat elemen. Penggunaan pointer untuk mengacu elemen
		 * berakibat elemen-elemen bersebelahan secara logik walau tidak bersebelahan
		 * secara fisik di memori.
		 * 
		 * - Link list adalah desain tempat penyimpanan data yang terdiri dari node-node
		 * (simpul-simpul) yang saling terhubung. - Link list dapat diilustrasikan
		 * seperti kereta api, dimana kereta api terdiri dari gerbong-gerbong yang
		 * saling terhubung yang dapat mengangkut penumpang. Gerbong disini setara
		 * dengan node dalam link list yang berfungsi untuk menyimpan data.
		 * 
		 * - Operasi-operasi yang bisa dilakukan dalam link list yaitu:
		 * 
		 * 1. Tambah data (insert) 2. Edit data (edit) 3. Hapus data (delete) 4.
		 * Pengurutan data (sorting) 5. Pencarian data (searching)
		 */
		LinkedList1();
	}

	public static void LinkedList1() {
		// create an Linked list
		LinkedList<String> electronic = new LinkedList<String>();

		// add elements to the Linked list
		electronic.add("Lamp");
		electronic.add("Handphone");
		electronic.add("Washing Machine");
		electronic.addLast("Computer");
		electronic.addFirst("Refrigerator");

		// Add an element into index=1
		electronic.add(1, "lamps");

		// display the Linked list
		System.out.println("Daftar electronic Awal \t\t\t\t\t: " + electronic);

		// Sorting
		electronic.sort(String::compareToIgnoreCase);
//		electronic.sort(String::);
		System.out.println("Daftar electronic setelah di sort  \t\t\t: " + electronic);

		// remove
		electronic.remove("handphone");
		electronic.remove(2);

		// display the Linked list after remove
		System.out.println("Daftar electronic setelah penghapusan \t\t\t: " + electronic);

		// remove firt element
		electronic.removeFirst();

		// remove last element
		electronic.removeLast();

		// display the Linked list after remove
		System.out.println("electronic setelah penghapusan pertama dan terakhir \t: " + electronic);

		// Edit data
		String val = electronic.get(2);
		electronic.set(2, val + " berubah");

		// display the Linked list after edit
		System.out.println("electronic setelah berubah \t\t\t\t: " + electronic);

	}
}
