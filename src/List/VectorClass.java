package List;

import java.util.Vector;

public class VectorClass {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * Menggunakan Vector mirip dengan menggunakan ArrayList. Perbedaannya adalah
		 * nama metode yang berbeda untuk melakukan tugas yang sama, atau nama metode
		 * yang berbeda untuk melakukan tugas yang sama. Seperti ArrayList, suatu Vector
		 * mirip dengan arrayObject yang bisa berkembang jika diperlukan. Konstruktor
		 * new Vector() membuat vektor tanpa elemen.
		 * 
		 * Metode pada vector diantaranya :
		 * 
		 * vec.size() adalah fungsi untuk mengembalikan jumlah elemen di dalam vektor.
		 * vec.addElement(obj) akan menambahkan Object obj di akhir vektor.
		 * vec.removeElement(obj) menghapus obj dari dalam vektor, kalau ada.
		 * vec.removeElementAt(N) menghapus elemen ke-N. N harus berada pada rentang 0
		 * hingga vec.size() � 1. Sama vec.setSize(N) akan mengubah ukuran vektor
		 * menjadi N. Jika di dalam vektor terdapat elemen yang jumlahnya lebih banyak
		 * dari N, maka elemen lainnya akan dihapus.
		 */
		vector1();

	}

	public static void vector1() {
		// create an Vektor
		Vector<Integer> vec = new Vector<Integer>();

		vec.add(22);

		vec.add(10);

		vec.add(15);

		System.out.println("jumlah elemen vektor : " + vec.size());

		System.out.println("cetak isi vektor : ");

		for (int i = 0; i < vec.size(); i++) {

			System.out.println("" + vec.get(i));

		}

	}

}
