package List;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import struts.dao.CityDao;
import struts.form.CityForm;
import struts.setting.DBConnection;

public class ArrayListClass {
	Connection conn;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * ArrayList adalah sebuah kelas yang dapat melakukan penyimpanan data berupa
		 * list objek berbentuk array yang ukurannya dapat berubah secara dinamis sesuai
		 * dengan jumlah data yang dimasukkan.
		 * 
		 * 
		 * Perbedaan paling mendasar antara Array dan ArrayList adalah:
		 * 
		 * Untuk menyimpan data dalam array biasa, maka harus mendeklarasikan jumlah
		 * elemen maksimal yang bisa menampung. Dengan kata lain jika jumlah datanya
		 * fleksibel, maka array tidak bisa digunakan. ArrayList dapat menampung
		 * sejumlah data secara dinamis, sehingga seberapapun jumlahnya akan ditampung
		 * oleh ArrayList tanpa memperhatikan berapa jumlah maksimal elemen yang dapat
		 * ditampung. ArrayList digunakan dalam menyimpan data dalam bentuk objek,
		 * sehingga untuk menyimpan data didalam ArrayList maka, buatlah sebuah kelas
		 * yang kemudian dijadikan objek yang dapat menyimpan data. ArrayList terdapat
		 * pada kelas java.util, sehingga untuk menggunakan ArrayList, maka harus
		 * melakukan import java.util.
		 */
		ArrayList1();
	}

	public static void ArrayList1() {
		// create an array list
		ArrayList<String> Electronic = new ArrayList<String>();
		System.out.println("Initial size of Electronic before add \t : " + Electronic.size());

		// add elements to the array list
		Electronic.add("Laptop");
		Electronic.add("Rice Cooker");
		Electronic.add("Fan");
		Electronic.add("Vacum Cleaner");

		// display the array list
		System.out.println("Contents of Electronic \t\t : " + Electronic);

		// Add an element into index=1
		Electronic.add("1000");
		System.out.println("\"1000\" was inserted \t\t : " + Electronic);
		System.out.println("Size of Electronic now \t\t : " + Electronic.size());
		Electronic.add("AC");
		Electronic.add("CCTV");
		Electronic.add("Handphone");
		System.out.println("Contents of Electronic \t\t : " + Electronic);

		// IndexOf
		System.out.println("The index of \"C\" \t\t : " + Electronic.indexOf("C"));

		// display ArrayList vertically by using Iterator
		System.out.println("\nDisplay ArrayList Vertically by using Iterator");
		Iterator<String> iterator = Electronic.iterator();
		while (iterator.hasNext()) {
			String element = (String) iterator.next();
			System.out.println(element);
		}

		// clear
		Electronic.clear();
		System.out.println("\nContents of Electronic after clear \t\t : " + Electronic);

	}
	
	//on cityForm
	private ArrayList<HashMap<String, String>> listCity;
	
	//DAO
	public ArrayList<HashMap<String, String>> getListAllCity() throws Exception{
		ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> map;
		PreparedStatement ps;
		ResultSet rs = null;
//		String sql = "SELECT * FROM mst_city";
		String sql = "SELECT city.city_id, city.city_name, prov.province_id, prov.province_name FROM mst_city city"
				+ " LEFT OUTER JOIN mst_province prov ON city.province_id = prov.province_id ORDER BY city_name";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()) {
				map = new HashMap<>();
				map.put("cityId", rs.getString("city_id"));
				map.put("cityName", rs.getString("city_name"));
				map.put("provinceId", rs.getString("province_id"));
				map.put("provinceName", rs.getString("province_name"));
				list.add(map);
			}
			rs.close();
			ps.close();
		} finally {
			conn.close();
		}
		return list;
	}
	//Service
	public void getListCity(CityForm frm) {
		CityDao dao = new CityDao();
		try {
			frm.setListCity(dao.getListAllCity());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	//

}
