package CastingNKonversi;

import java.util.ArrayList;
import java.util.List;

public class CastingNKonversion {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		KonversiIntToString();
		KonversiStringToInt();
		KonversiDoubleToInteger();
	}
//	1. Overview of Primitives
	/*
	 * byte  8 bits and signed
	 * 
	 * short  16 bits and signed
	 * 
	 * char  16 bits and unsigned, so that it may represent Unicode characters
	 * 
	 * int  32 bits and signed
	 * 
	 * long  64 bits and signed
	 * 
	 * float  32 bits and signed
	 * 
	 * double  64 bits and signed
	 * 
	 * boolean  its not numeric, may only have true or false values
	 */
	public static void PrimitiveConversions() {
		// Widening Primitive Conversions
		int myInt = 127;
		long myLong = myInt;
		
		float myFloat = myLong;
		double myDouble = myLong;
		
		// Narrowing Primitive Conversion
		int myInt2 = (int) myDouble;
		byte myByte = (byte) myInt2;
		
		//Widening and Narrowing Primitive Conversion
		byte myLargeValueByte = (byte) 130;   //0b10000010 -126
		
		char myLargeValueChar = (char) myLargeValueByte; //0b11111111 10000010 unsigned value
		int myLargeValueInt = myLargeValueChar; //0b11111111 10000010 65410
		
		byte myOtherByte = (byte) myLargeValueInt; //0b10000010 -126
		
		char myLargeValueChar2 = 130; //This is an int not a byte! //0b 00000000 10000010 unsigned value
		         
		int myLargeValueInt2 = myLargeValueChar2; //0b00000000 10000010  130
		         
		byte myOtherByte2 = (byte) myLargeValueInt2; //0b10000010 -126
		
		// Boxing/Unboxing Conversion
		Integer myIntegerReference = myInt;
		int myOtherInt = myIntegerReference;
		
		// String Conversions
		String myString = myIntegerReference.toString();
		
		byte  myNewByte   = Byte.parseByte(myString);
		short myNewShort  = Short.parseShort(myString);
		int   myNewInt    = Integer.parseInt(myString);
		long  myNewLong   = Long.parseLong(myString);
		 
		float  myNewFloat  = Float.parseFloat(myString);
		double myNewDouble = Double.parseDouble(myString);
		
		boolean myNewBoolean = Boolean.parseBoolean(myString);
		
		char myNewChar = myString.charAt(0);
		
		// Numeric Promotions
		byte op1 = 4;
		byte op2 = 5;
		byte myResultingByte = (byte) ((byte) op1 + op2);
		
	}
	//========================================================================================
//	2. Primitive vs. Reference
	public static String KonversiIntToString() {
		int nilai = 60;
		String hasilCast = String.valueOf(nilai);// Casting dari int ke String
		System.out.println("Konversi dari int ke String \t:" + nilai + " \t->\t " + hasilCast);
		return hasilCast;
	}

	public static Integer KonversiStringToInt() {
		String nilai = "60";
		int hasilCast = Integer.valueOf(nilai); // Casting dati String ke int
		System.out.println("Konversi dati String ke int \t:'" + nilai + "' \t->\t " + hasilCast);
		return hasilCast;
	}

	public static Integer KonversiDoubleToInteger() {
		double myDouble = 1.1;
		int myInt = (int) myDouble; // Casting dati int ke double
		System.out.println("Konversi dati int ke double \t:" + myDouble + " \t->\t " + myInt);
		return myInt;
	}
	//========================================================================================
//	3. Upcasting
	public class Animal {
		 
	    public void eat() {
	        System.out.println("Animal makan something"); 
	    }
	}
	public class Cat extends Animal {
		 
	    public void eat() {
	         System.out.println("Cat makan Ikan");
	    }
	 
	    public void meow() {
	         System.out.println("Cat bunyi meow");
	    }
	}
	public void Upcasting() {
		Cat cat = new Cat();
		
		Animal animal = cat;
		
		animal = (Animal) cat;
		
		// animal.meow(); The method meow() is undefined for the type Animal
	}
	//========================================================================================
//	3.1. Polymorphism
	public class Dog extends Animal {
		 
	    public void eat() {
	    	System.out.println("Dog makan Tulang");
	    }
	}
	public class AnimalFeeder {
		 
	    public void feed(List<Animal> animals) {
	        animals.forEach(animal -> {
	            animal.eat();
	        });
	    }
	}
	public void Polymorphism() {
		List<Animal> animals = new ArrayList<>();
		animals.add(new Cat());
		animals.add(new Dog());
		new AnimalFeeder().feed(animals);
		
		Object object = new Animal();
	}
	
	//========================================================================================
	public interface Mew {
	    public void meow();
	}
	 
	public class Cat2 extends Animal implements Mew {
	     
	    public void eat() {
	    	System.out.println("Cat2 makan Ikan");
	    }
	 
	    public void meow() {
	    	System.out.println("Cat2 bunyi meow");
	    }
	}
	public void pakai() {
		Mew mew = new Cat2();
	}
	
	//========================================================================================
//	3.2. Overriding
	public void feed(List<Animal> animals) {
	    animals.forEach(animal -> {
	        animal.eat();
	    });
	}
	
	//========================================================================================
//	4. Downcasting
//	Animal animal = new Cat();
//	((Cat) animal).meow();
	
	public class AnimalFeeder2 {
		 
	    public void feed(List<Animal> animals) {
	        animals.forEach(animal -> {
	            animal.eat();
	            if (animal instanceof Cat) {
	                ((Cat) animal).meow();
	            }
	        });
	    }
	}
	
	

}
