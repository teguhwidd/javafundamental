package struts.service;

import struts.dao.CityDao;
import struts.form.CityForm;

public class CityService {
	public void getListCity(CityForm frm) {
		CityDao dao = new CityDao();
		try {
			frm.setListCity(dao.getListAllCity());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
