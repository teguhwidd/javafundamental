package struts.form;

import org.apache.struts.action.ActionForm;

public class BaseForm extends ActionForm{
	private String task;
	
//	private String nextPage;
//	private String prevPage;
//	private String firstPage;
//	private String lastPage;
//	private String gotoPage;
//	private String nPage;
//	private String totPage;
//	private String totRecord;

	public String getTask() {
		return task;
	}
	public void setTask(String task) {
		this.task = task;
	}
}