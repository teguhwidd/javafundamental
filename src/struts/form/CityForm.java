package struts.form;

import java.util.ArrayList;
import java.util.HashMap;

public class CityForm extends BaseForm {
	private String cityId;
	private String cityName;
	private String searchCityName;
	private String provinceId;
	private String provinceName;
	private String field;
	private ArrayList<HashMap<String, String>> listCity;

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getSearchCityName() {
		return searchCityName;
	}

	public void setSearchCityName(String searchCityName) {
		this.searchCityName = searchCityName;
	}

	public ArrayList<HashMap<String, String>> getListCity() {
		return listCity;
	}

	public void setListCity(ArrayList<HashMap<String, String>> listCity) {
		this.listCity = listCity;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(String provinceId) {
		this.provinceId = provinceId;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
}
