package struts.setting;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.sql.DataSource;

public class DBConnection {
//	public static String DS_NAME = "java:jboss/datasources/ds_oracle_hr"; //for jboss
	public static String DS_NAME = "ds_oracle_hr"; //for WebLogic
	
	public static Connection getConnection() throws Exception{
		Connection conn = null;
		InitialContext ctx = new InitialContext();
		try {
			DataSource ds = (DataSource)ctx.lookup(DS_NAME);
			conn = ds.getConnection();
		}catch(SQLException ex) {
			ex.printStackTrace();
		}finally {
			ctx.close();
		}
		return conn;
	}
	public static Connection getConReport() throws Exception{
		Connection conn = null;
		
		String hostname = "localhost";
		String port = "1521";
		String sid = "xe";
		String username = "hr";
		String password = "12345";
		
		try {
			DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
			conn = DriverManager.getConnection("jdbc:oracle:thin:@" + hostname + ":" + port + ":" + sid, username, password);
		}
		catch(SQLException ex) {
			ex.printStackTrace();
		}
		return conn;
	}
}
