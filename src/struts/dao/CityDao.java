package struts.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import struts.setting.DBConnection;

public class CityDao {
	Connection conn;
	
	public ArrayList<HashMap<String, String>> getListAllCity() throws Exception{
		ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> map;
		PreparedStatement ps;
		ResultSet rs = null;
//		String sql = "SELECT * FROM mst_city";
		String sql = "SELECT city.city_id, city.city_name, prov.province_id, prov.province_name FROM mst_city city"
				+ " LEFT OUTER JOIN mst_province prov ON city.province_id = prov.province_id ORDER BY city_name";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()) {
				map = new HashMap<>();
				map.put("cityId", rs.getString("city_id"));
				map.put("cityName", rs.getString("city_name"));
				map.put("provinceId", rs.getString("province_id"));
				map.put("provinceName", rs.getString("province_name"));
				list.add(map);
			}
			rs.close();
			ps.close();
		} finally {
			conn.close();
		}
		return list;
	}
}
