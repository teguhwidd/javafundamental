package Map;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class HashMapClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * Always use interface type (Map), generics and diamond operator to declare a
		 * new map. Because a Map is not a true collection, its characteristics and
		 * behaviors are different than the other collections like List or Set. A Map
		 * cannot contain duplicate keys and each key can map to at most one value. Some
		 * implementations allow null key and null value (HashMap and LinkedHashMap) but
		 * some does not (TreeMap).
		 */
		/*
		 * Maps are perfectly for key-value association mapping such as dictionaries.
		 * Use Maps when you want to retrieve and update elements by keys, or perform
		 * lookups by keys. Some examples: A map of error codes and their descriptions.
		 * A map of zip codes and cities. A map of managers and employees. Each manager
		 * (key) is associated with a list of employees (value) he manages. A map of
		 * classes and students. Each class (key) is associated with a list of students
		 * (value).
		 */
		/*
		 * In the inheritance tree of the Map interface, there are several
		 * implementations but only 3 major, common, and general purpose implementations
		 * - they are HashMap and LinkedHashMap and TreeMap. Let�s see the
		 * characteristics and behaviors of each implementation: @HashMap: this
		 * implementation uses a hash table as the underlying data structure. It
		 * implements all of the Map operations and allows null values and one null key.
		 * This class is roughly equivalent to Hashtable - a legacy data structure
		 * before Java Collections Framework, but it is not synchronized and permits
		 * nulls. HashMap does not guarantee the order of its key-value elements.
		 * Therefore, consider to use a HashMap when order does not matter and nulls are
		 * acceptable. @LinkedHashMap: this implementation uses a hash table and a linked
		 * list as the underlying data structures, thus the order of a LinkedHashMap is
		 * predictable, with insertion-order as the default order. This implementation
		 * also allows nulls like HashMap. So consider using a LinkedHashMap when you
		 * want a Map with its key-value pairs are sorted by their insertion order.
		 * @TreeMap: this implementation uses a red-black tree as the underlying data
		 * structure. A TreeMap is sorted according to the natural ordering of its keys,
		 * or by a Comparator provided at creation time. This implementation does not
		 * allow nulls. So consider using a TreeMap when you want a Map sorts its
		 * key-value pairs by the natural order of the keys (e.g. alphabetic order or
		 * numeric order), or by a custom order you specify. So far you have understood
		 * the key differences of the 3 major Map�s implementations. And the code
		 * examples in this tutorial are around them.
		 */

		hashMap1();
	}

	public static void hashMap1() {
		// Creating a HashMap:
		Map<Integer, String> mapHttpErrors = new HashMap<>();

		mapHttpErrors.put(200, "OK");
		mapHttpErrors.put(303, "See Other");
		mapHttpErrors.put(404, "Not Found");
		mapHttpErrors.put(500, "Internal Server Error");

		System.out.println("isi HashMap \t\t\t : " + mapHttpErrors);

		Map<String, String> mapCountryCodes = new HashMap<>();

		mapCountryCodes.put("1", "USA");
		mapCountryCodes.put("44", "United Kingdom");
		mapCountryCodes.put("33", "France");
		mapCountryCodes.put("81", "Japan");

		Set<String> setCodes = mapCountryCodes.keySet();
		Iterator<String> iterator = setCodes.iterator();

		while (iterator.hasNext()) {
			String code = iterator.next();
			String country = mapCountryCodes.get(code);

			System.out.println(code + " => " + country);
		}
	}
}
