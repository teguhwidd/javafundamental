package Map;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

public class HashTableClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		hashTable1();
		/*
		 * perbedaan antara Hashtable dan HashMap.
		 * 
		 * Perbedaan yang paling mencolok adalah Hashtable adalah collection yang
		 * thread-safe dengankan HashMap tidak. Artinya apabila resource Hashtable
		 * digunakan dalam waktu yang bersamaan, maka dapat dipastikan bahwa data yang
		 * ada akan selalu memiliki hasil yang sama (datanya tersinkronisasi).
		 * Sedangkan, apabila kita menggunakan resource HashMap dalam waktu yang
		 * bersamaan, kita bisa saja mendapatkan hasil yang berbeda. Perbedaan kedua
		 * yang tak kalah penting adalah performanya. Karena HashMap tidak
		 * tersinkronisasi, maka ia memiliki performa yang lebih baik dibanding
		 * Hashtable(dalam hal kecepatan). Perbeddaan selanjutnya adalah bahwa Hastable
		 * Hashtable merupakan kelas obsolete yang artinya penggunaan Hashtable sudah
		 * tidak disarankan. Apabila ingin menggunakan Hashtable kita disarankan untuk
		 * menggunakan ConcurrentHasMap.
		 */
	}

	public static void hashTable1() {
		// ----------hashtable -------------------------
//        Hashtable<Integer,String> ht=new Hashtable<Integer,String>(); 
		Map<Integer, String> ht = new Hashtable<Integer, String>();
		ht.put(101, " ajay");
		ht.put(101, "Vijay");
		ht.put(102, "Ravi");
		ht.put(103, "Rahul");
		System.out.println("-------------Hash table--------------");
		for (Map.Entry temp : ht.entrySet()) {
			System.out.println(temp.getKey() + " " + temp.getValue());
		}

		// ----------------hashmap--------------------------------
//        Hashtable<Integer,String> hm=new HashMap<Integer,String>(); 
		Map<Integer, String> hm = new HashMap<Integer, String>();
		hm.put(100, "Amit");
		hm.put(104, "Amit"); // hash map allows duplicate values
		hm.put(101, "Vijay");
		hm.put(102, "Rahul");
		System.out.println("-----------Hash map-----------");
		for (Map.Entry m : hm.entrySet()) {
			System.out.println(m.getKey() + " " + m.getValue());
		}
	}

}
