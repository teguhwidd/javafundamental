package Map;

import java.util.Map;
import java.util.TreeMap;

public class TreeMapClass {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		treeMap1();
	}
	public static void treeMap1() {
		Map<String, String> mapLang = new TreeMap<>();
		 
		mapLang.put(".c", "C");
		mapLang.put(".java", "Java");
		mapLang.put(".pl", "Perl");
		mapLang.put(".cs", "C#");
		mapLang.put(".php", "PHP");
		mapLang.put(".cpp", "C++");
		mapLang.put(".xml", "XML");
		 
		System.out.println(mapLang);
	}
}
